[![Open Source Helpers](https://www.codetriage.com/wazedev/wme-send-to-slack/badges/users.svg)](https://www.codetriage.com/wazedev/wme-send-to-slack)

---What's doing this script ?---

It's more easy to send requests about Lock, Downlock, Closure, Openning, and Validation of changes
The requests are sent to a webhook to Slack or Discrord
The requests can be sent to a GForm too (currently only with the sames fields we have in BeLux)
Thanks to that and to Bertzzz's , we've got a spreadsheet that helps to promotion decision on L1-L3 levels

---What's New---
This script now supports States, and i've added the USA NewYork, New Jersay, ...

---What does it looks like ?---
 
The Validation link is on top of the Left collumn just above the settings tab

![Img](https://api.tipeee.com/cache/20191024193712/media/1753052/201910245db1e147f3dca.png)

The Lock and Downlock links are below the locking choice in the général tab

![Img](https://api.tipeee.com/cache/20191024193952/media/1753054/201910245db1e1e8436ad.png)

The Closure and Open links are ...? In the Closure Tab just above the Closure tab.
   Just one thing, this part only shows if the closure layer is set

![Img](https://api.tipeee.com/cache/20191024194111/media/1753058/201910245db1e237a2538.png)

---How can i use it ?---

Like most of the scripts used by Waze editors, you need to install 
A browser, but beeing here, i assume that's already the case...(Not Internet explorer or Edge)
Tampermonkey extention
The script itself... 
https://gitlab.com/WMEScripts/wme-send-to-slack-public/raw/master/WME-send-to-slack.user.js

---What's different whith the old one ?---

I've : 
- rewrited completely the script
- fixed some of the bugs
- moved the icons
- added a setting tab when a venue, segment, camera, JB, ... is selected
- added the ability to add more Countries on Slack/Discord
- updated the GForm submition
- the script source code is now in clear text except for one dependency

So now, If you want to add a Country/Community, if you're using Slack or Discord, just tell me...
If you just want to add other supports(other than Discord or Slack), let me know which, and i'll search to use it.

---How to send bugs or features ?---

Just :
go on this page (you'll need to create an account) : https://gitlab.com/WMEScripts/wme-send-to-slack-public/issues
or send it by mail at incoming+wmescripts-wme-send-to-slack-public-14393158-issue-@incoming.gitlab.com
The mail does not work with every mailbox

See you later for New things...


New informations here : https://en.tipeee.com/tunisiano18/news
